(function( $ ) {
 
    $.fn.accordion = function(options) {

        var defaults = {   
            debug:      false,
            speed:      5500,

            iconOpen:   'fa-angle-down',
            iconClosed: 'fa-angle-right'
        };
     
        var settings = $.extend( {}, defaults, options );

        this.each(function () {

            var self = $(this);

            var getSpeed = function(obj) {
                var speed = settings.speed;
                if(self.data('speed')) {
                    speed = self.data('speed');
                }
                return speed;
            }

            var openAccordion = function(obj) {
                self.find('li').removeClass('active').find('.m-accordion-text').slideUp(getSpeed());
                self.find('li').find('i').removeClass(settings.iconOpen).addClass(settings.iconClosed);

                obj.addClass('active').find('.m-accordion-text').slideDown(getSpeed());
                obj.addClass('active').find('i').removeClass(settings.iconClosed).addClass(settings.iconOpen);
            };

            var closeAccordion = function(obj) {
                self.find('li').removeClass('active').find('.m-accordion-text').slideUp(getSpeed());
                self.find('li').find('i').removeClass(settings.iconOpen).addClass(settings.iconClosed);
            };

            openAccordion( self.find('li.active') );

            $(this).find('li').off().on('click', function(e) { 
                e.preventDefault();

                if($(this).hasClass('active')) {
                    closeAccordion( $(this) );
                }else{
                    openAccordion( $(this) );
                }
            });
        });

        return this;
    };

}( jQuery ));